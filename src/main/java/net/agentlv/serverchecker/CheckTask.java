package net.agentlv.serverchecker;

import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author AgentLV
 */
public class CheckTask implements Runnable {

    private final Map<ServerInfo, Boolean> servers = new HashMap<>();
    private final ServerChecker serverChecker;

    public CheckTask(ServerChecker serverChecker) {
        this.serverChecker = serverChecker;

        for (ServerInfo serverInfo : serverChecker.getProxy().getServers().values()) {
            servers.put(serverInfo, false); // All servers are offline by default
        }
    }

    @Override
    public void run() {
        for (final ServerInfo serverInfo : serverChecker.getProxy().getServers().values()) {
            boolean skip = serverChecker.isWhitelist();

            for (String serverName : serverChecker.getServerList()) {
                if (serverName.equals(serverInfo.getName())) {
                    skip = !serverChecker.isWhitelist();

                    break;
                }
            }

            if (skip) {
                continue;
            }

            serverInfo.ping(new Callback<ServerPing>() {
                @Override
                public void done(ServerPing sp, Throwable ex) {
                    boolean online = ex == null;

                    if (!servers.containsKey(serverInfo)) {
                        servers.put(serverInfo, online);
                    } else if (servers.get(serverInfo) != online) {
                        String[] configMessage = online ? serverChecker.getOnline_msg() : serverChecker.getOffline_msg();
                        BaseComponent[][] message = new BaseComponent[configMessage.length][];

                        for (int i = 0; i < configMessage.length; ++i) {
                            message[i] = TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', configMessage[i].replaceAll("%server%", serverInfo.getName())));
                        }

                        for (ProxiedPlayer p : serverChecker.getProxy().getPlayers()) {
                            if (p.hasPermission("serverchecker.notify")) {
                                for (BaseComponent[] msg : message) {
                                    p.sendMessage(ChatMessageType.CHAT, msg);
                                    // p.sendTitle(ProxyServer.getInstance().createTitle().reset().title(message));
                                }
                            }
                        }

                        servers.put(serverInfo, online);
                    }
                }
            });
        }
    }
}
