package net.agentlv.serverchecker;

import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.TimeUnit;

/**
 * @author AgentLV
 */
public class ServerChecker extends Plugin {

    @Getter private String[] online_msg;
    @Getter private String[] offline_msg;
    @Getter private String[] serverList;
    @Getter private boolean whitelist;
    private int interval;
    private ScheduledTask scheduledTask;

    @Override
    public void onEnable() {
        initConfigs();

        CheckTask checkTask = new CheckTask(this);

        scheduledTask = getProxy().getScheduler().schedule(this, checkTask, 0, interval, TimeUnit.SECONDS);
    }

    @Override
    public void onDisable() {
        scheduledTask.cancel();
    }

    private void initConfigs() {
        // Create plugin directory if necessary
        if (!getDataFolder().exists() && !getDataFolder().mkdir()) {
            getLogger().severe("Could not create data folder!");
            return;
        }

        /*---------- config.yml ----------*/
        Configuration cfg;
        try {
            cfg = loadConfig("config.yml");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        interval = cfg.getInt("interval");
        online_msg = cfg.getStringList("online_msg").toArray(new String[cfg.getStringList("online_msg").size()]);
        offline_msg = cfg.getStringList("offline_msg").toArray(new String[cfg.getStringList("offline_msg").size()]);

        /*---------- servers.yml ----------*/
        try {
            cfg = loadConfig("servers.yml");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        whitelist = cfg.getString("type").equalsIgnoreCase("whitelist");
        serverList = cfg.getStringList("servers").toArray(new String[cfg.getStringList("servers").size()]);
    }

    private Configuration loadConfig(String fileName) throws IOException {
        File file = new File(getDataFolder(), fileName);

        if (!file.exists()) {
            Files.copy(getResourceAsStream(fileName), file.toPath());
        }

        return ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
    }

}
